function getCustomerConfig(data) {
	
	var resp;
	if(Array.isArray(data.CustomerId)){
		var responses = [];
		for (var i = 0; i < data.CustomerId.length; i++) {
		responses.push(sendRequest(data, i));
	}
	resp = { 'adapterResponse': responses };
	}
	else{
		resp = getSingleCustomerConfig(data);
	}

	return resp;
}

function getSingleCustomerConfig(data) {
	var input = {
	    method : 'GET',
	    returnedContentType : 'json',
	    path: getCustomerConfigString(data,'CRCF',-1),
	};
	return WL.Server.invokeHttp(input);
}

function sendRequest(data,index){
	var input = {
	    method : 'GET',
	    returnedContentType : 'json',
	    path: getCustomerConfigString(data,'CRCF',index),
	};
	var returnData = {
		'serverResponse': WL.Server.invokeHttp(input),
		'path': input.path,
		'_custId': data.CustomerId[index]
	};
	return returnData;
}

function getCustomerConfigString(data, eventModifier,index) {
	//Parse the data object and build the string below
	var curDat = new Date().toISOString();
	var req = '/v10/InfoRequest/';
	req += eventModifier;
	req += '/Beam/';
	req += data.DeviceInfo.AppVersion+'/';
	req += data.DriverInfo.FacilityID+'/';
	req += data.DriverInfo.DriverID+'/';
	req += data.EventISODateTime+'/';
	req += data.LocationInfo.Longitude+'/';
	req += data.LocationInfo.Latitude+'/';
	req += data.LocationInfo.GPSFixDateTime+'/';
	req += data.LocationInfo.GPSPrecision+'/';
	req += data.SecurityGuid+'/';
	req += data.DeviceInfo.DeviceID+'/';
	req += data.DeviceInfo.PhoneIp+'/';
	req += data.DeviceInfo.PhoneNum+'/';
	req += data.DriverInfo.DriverID;
	req += '////';
	req += (index>-1?data.CustomerId[index]:data.CustomerId);
	req += '///////';
	return req;
}