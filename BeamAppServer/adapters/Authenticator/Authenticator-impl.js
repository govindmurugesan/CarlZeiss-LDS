function getLogin(params) {
	var requestString = getLoginString(params,'LOGN');
	var input = {
		method: 'POST',
		path: requestString,
		body: {
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			content: 'Password=' + params.DriverInfo.Password, //Password from data
		},
	};
	//return {'str':requestString};
	return WL.Server.invokeHttp(input);
}

function getLogout(data) {
	var requestString =getLoginString(data,'LOGT');
	var input = {
		method: 'GET',
		path: requestString,
	};
	//return {'str':requestString};
	return WL.Server.invokeHttp(input);
}

function getLoginString(data, eventModifier) {
	//Parse the data object and build the string below
	var curDat = new Date().toISOString();
	var req = '/v10/InfoRequest/';
	req += eventModifier;
	req += '/Beam/';
	req += data.DeviceInfo.AppVersion+'/';
	req += data.DriverInfo.FacilityID+'/';
	req += data.DriverInfo.DriverID+'/';
	req += data.EventISODateTime+'/';
	req += data.LocationInfo.Longitude+'/';
	req += data.LocationInfo.Latitude+'/';
	req += data.LocationInfo.GPSFixDateTime+'/';
	req += data.LocationInfo.GPSPrecision+'/';
	req += data.SecurityGuid+'/';
	req += data.DeviceInfo.DeviceID+'/';
	req += data.DeviceInfo.PhoneIp+'/';
	req += data.DeviceInfo.PhoneNum+'/';
	req += data.DriverInfo.DriverID;
	req += '///////////';
	return req;
}