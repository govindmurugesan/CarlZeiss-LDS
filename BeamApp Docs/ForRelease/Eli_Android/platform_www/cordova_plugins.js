cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/barcode.js",
        "id": "com.mirasense.scanditsdk.plugin.Barcode",
        "pluginId": "com.mirasense.scanditsdk.plugin",
        "clobbers": [
            "Scandit.Barcode"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/barcodepicker.js",
        "id": "com.mirasense.scanditsdk.plugin.BarcodePicker",
        "pluginId": "com.mirasense.scanditsdk.plugin",
        "clobbers": [
            "Scandit.BarcodePicker"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/license.js",
        "id": "com.mirasense.scanditsdk.plugin.License",
        "pluginId": "com.mirasense.scanditsdk.plugin",
        "clobbers": [
            "Scandit.License"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/margins.js",
        "id": "com.mirasense.scanditsdk.plugin.Margins",
        "pluginId": "com.mirasense.scanditsdk.plugin",
        "clobbers": [
            "Scandit.Margins"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/point.js",
        "id": "com.mirasense.scanditsdk.plugin.Point",
        "pluginId": "com.mirasense.scanditsdk.plugin",
        "clobbers": [
            "Scandit.Point"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/quadrilateral.js",
        "id": "com.mirasense.scanditsdk.plugin.Quadrilateral",
        "pluginId": "com.mirasense.scanditsdk.plugin",
        "clobbers": [
            "Scandit.Quadrilateral"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/rect.js",
        "id": "com.mirasense.scanditsdk.plugin.Rect",
        "pluginId": "com.mirasense.scanditsdk.plugin",
        "clobbers": [
            "Scandit.Rect"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/overlay.js",
        "id": "com.mirasense.scanditsdk.plugin.ScanOverlay",
        "pluginId": "com.mirasense.scanditsdk.plugin",
        "clobbers": [
            "Scandit.ScanOverlay"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/session.js",
        "id": "com.mirasense.scanditsdk.plugin.ScanSession",
        "pluginId": "com.mirasense.scanditsdk.plugin",
        "clobbers": [
            "Scandit.ScanSession"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/settings.js",
        "id": "com.mirasense.scanditsdk.plugin.ScanSettings",
        "pluginId": "com.mirasense.scanditsdk.plugin",
        "clobbers": [
            "Scandit.ScanSettings"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/symbologysettings.js",
        "id": "com.mirasense.scanditsdk.plugin.SymbologySettings",
        "pluginId": "com.mirasense.scanditsdk.plugin",
        "clobbers": [
            "Scandit.SymbologySettings"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/constraints.js",
        "id": "com.mirasense.scanditsdk.plugin.Constraints",
        "pluginId": "com.mirasense.scanditsdk.plugin",
        "clobbers": [
            "Scandit.Constraints"
        ]
    },
    {
        "file": "plugins/cordova-plugin-insomnia/www/Insomnia.js",
        "id": "cordova-plugin-insomnia.Insomnia",
        "pluginId": "cordova-plugin-insomnia",
        "clobbers": [
            "window.plugins.insomnia"
        ]
    },
    {
        "file": "plugins/cordova-plugin-mfp/bootstrap.js",
        "id": "cordova-plugin-mfp.mfp",
        "pluginId": "cordova-plugin-mfp",
        "runs": true
    },
    {
        "file": "plugins/cordova-plugin-mfp-jsonstore/bootstrap.js",
        "id": "cordova-plugin-mfp-jsonstore.jsonstore",
        "pluginId": "cordova-plugin-mfp-jsonstore",
        "runs": true
    },
    {
        "file": "plugins/cordova-plugin-networkinterface/www/networkinterface.js",
        "id": "cordova-plugin-networkinterface.networkinterface",
        "pluginId": "cordova-plugin-networkinterface",
        "clobbers": [
            "window.networkinterface"
        ]
    },
    {
        "file": "plugins/cordova-plugin-screen-orientation/www/screenorientation.js",
        "id": "cordova-plugin-screen-orientation.screenorientation",
        "pluginId": "cordova-plugin-screen-orientation",
        "clobbers": [
            "cordova.plugins.screenorientation"
        ]
    },
    {
        "file": "plugins/cordova-plugin-screen-orientation/www/screenorientation.android.js",
        "id": "cordova-plugin-screen-orientation.screenorientation.android",
        "pluginId": "cordova-plugin-screen-orientation",
        "merges": [
            "cordova.plugins.screenorientation"
        ]
    },
    {
        "file": "plugins/cordova.plugins.diagnostic.api-22/www/android/diagnostic.js",
        "id": "cordova.plugins.diagnostic.api-22.Diagnostic",
        "pluginId": "cordova.plugins.diagnostic.api-22",
        "clobbers": [
            "cordova.plugins.diagnostic"
        ]
    },
    {
        "file": "plugins/ionic-plugin-keyboard/www/android/keyboard.js",
        "id": "ionic-plugin-keyboard.keyboard",
        "pluginId": "ionic-plugin-keyboard",
        "clobbers": [
            "cordova.plugins.Keyboard"
        ],
        "runs": true
    },
    {
        "file": "plugins/org.apache.cordova.battery-status/www/battery.js",
        "id": "org.apache.cordova.battery-status.battery",
        "pluginId": "org.apache.cordova.battery-status",
        "clobbers": [
            "navigator.battery"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.device-motion/www/Acceleration.js",
        "id": "org.apache.cordova.device-motion.Acceleration",
        "pluginId": "org.apache.cordova.device-motion",
        "clobbers": [
            "Acceleration"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.device-motion/www/accelerometer.js",
        "id": "org.apache.cordova.device-motion.accelerometer",
        "pluginId": "org.apache.cordova.device-motion",
        "clobbers": [
            "navigator.accelerometer"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.splashscreen/www/splashscreen.js",
        "id": "org.apache.cordova.splashscreen.SplashScreen",
        "pluginId": "org.apache.cordova.splashscreen",
        "clobbers": [
            "navigator.splashscreen"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.statusbar/www/statusbar.js",
        "id": "org.apache.cordova.statusbar.statusbar",
        "pluginId": "org.apache.cordova.statusbar",
        "clobbers": [
            "window.StatusBar"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.vibration/www/vibration.js",
        "id": "org.apache.cordova.vibration.notification",
        "pluginId": "org.apache.cordova.vibration",
        "merges": [
            "navigator.notification",
            "navigator"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.device/www/device.js",
        "id": "org.apache.cordova.device.device",
        "pluginId": "org.apache.cordova.device",
        "clobbers": [
            "device"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.dialogs/www/notification.js",
        "id": "org.apache.cordova.dialogs.notification",
        "pluginId": "org.apache.cordova.dialogs",
        "merges": [
            "navigator.notification"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.dialogs/www/android/notification.js",
        "id": "org.apache.cordova.dialogs.notification_android",
        "pluginId": "org.apache.cordova.dialogs",
        "merges": [
            "navigator.notification"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.globalization/www/GlobalizationError.js",
        "id": "org.apache.cordova.globalization.GlobalizationError",
        "pluginId": "org.apache.cordova.globalization",
        "clobbers": [
            "window.GlobalizationError"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.globalization/www/globalization.js",
        "id": "org.apache.cordova.globalization.globalization",
        "pluginId": "org.apache.cordova.globalization",
        "clobbers": [
            "navigator.globalization"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.inappbrowser/www/inappbrowser.js",
        "id": "org.apache.cordova.inappbrowser.inappbrowser",
        "pluginId": "org.apache.cordova.inappbrowser",
        "clobbers": [
            "window.open"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.network-information/www/network.js",
        "id": "org.apache.cordova.network-information.network",
        "pluginId": "org.apache.cordova.network-information",
        "clobbers": [
            "navigator.connection",
            "navigator.network.connection"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.network-information/www/Connection.js",
        "id": "org.apache.cordova.network-information.Connection",
        "pluginId": "org.apache.cordova.network-information",
        "clobbers": [
            "Connection"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "com.mirasense.scanditsdk.plugin": "4.13.4",
    "cordova-plugin-android-support-v4": "21.0.1",
    "cordova-plugin-insomnia": "4.2.0",
    "cordova-plugin-mfp": "7.1.0",
    "cordova-plugin-mfp-jsonstore": "7.1.0",
    "cordova-plugin-mfp-push": "7.1.0",
    "cordova-plugin-networkinterface": "1.0.8",
    "cordova-plugin-screen-orientation": "1.4.0",
    "cordova.plugins.diagnostic.api-22": "2.3.10-api-22",
    "ionic-plugin-keyboard": "2.2.1",
    "org.apache.cordova.battery-status": "0.2.12",
    "org.apache.cordova.console": "0.2.12",
    "org.apache.cordova.device-motion": "0.2.11",
    "org.apache.cordova.splashscreen": "0.3.5",
    "org.apache.cordova.statusbar": "0.1.9",
    "org.apache.cordova.vibration": "0.3.12",
    "org.apache.cordova.device": "0.2.13",
    "org.apache.cordova.dialogs": "0.2.11",
    "org.apache.cordova.geolocation": "0.3.11",
    "org.apache.cordova.globalization": "0.3.3",
    "org.apache.cordova.inappbrowser": "0.5.4",
    "org.apache.cordova.network-information": "0.2.14"
}
// BOTTOM OF METADATA
});