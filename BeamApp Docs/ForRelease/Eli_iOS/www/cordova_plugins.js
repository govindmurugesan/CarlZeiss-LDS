cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/barcode.js",
        "id": "com.mirasense.scanditsdk.plugin.Barcode",
        "clobbers": [
            "Scandit.Barcode"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/barcodepicker.js",
        "id": "com.mirasense.scanditsdk.plugin.BarcodePicker",
        "clobbers": [
            "Scandit.BarcodePicker"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/license.js",
        "id": "com.mirasense.scanditsdk.plugin.License",
        "clobbers": [
            "Scandit.License"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/margins.js",
        "id": "com.mirasense.scanditsdk.plugin.Margins",
        "clobbers": [
            "Scandit.Margins"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/point.js",
        "id": "com.mirasense.scanditsdk.plugin.Point",
        "clobbers": [
            "Scandit.Point"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/quadrilateral.js",
        "id": "com.mirasense.scanditsdk.plugin.Quadrilateral",
        "clobbers": [
            "Scandit.Quadrilateral"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/rect.js",
        "id": "com.mirasense.scanditsdk.plugin.Rect",
        "clobbers": [
            "Scandit.Rect"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/overlay.js",
        "id": "com.mirasense.scanditsdk.plugin.ScanOverlay",
        "clobbers": [
            "Scandit.ScanOverlay"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/session.js",
        "id": "com.mirasense.scanditsdk.plugin.ScanSession",
        "clobbers": [
            "Scandit.ScanSession"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/settings.js",
        "id": "com.mirasense.scanditsdk.plugin.ScanSettings",
        "clobbers": [
            "Scandit.ScanSettings"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/symbologysettings.js",
        "id": "com.mirasense.scanditsdk.plugin.SymbologySettings",
        "clobbers": [
            "Scandit.SymbologySettings"
        ]
    },
    {
        "file": "plugins/com.mirasense.scanditsdk.plugin/src/constraints.js",
        "id": "com.mirasense.scanditsdk.plugin.Constraints",
        "clobbers": [
            "Scandit.Constraints"
        ]
    },
    {
        "file": "plugins/cordova-plugin-insomnia/www/Insomnia.js",
        "id": "cordova-plugin-insomnia.Insomnia",
        "clobbers": [
            "window.plugins.insomnia"
        ]
    },
    {
        "file": "plugins/cordova-plugin-mfp/bootstrap.js",
        "id": "cordova-plugin-mfp.mfp",
        "runs": true
    },
    {
        "file": "plugins/cordova-plugin-mfp-jsonstore/bootstrap.js",
        "id": "cordova-plugin-mfp-jsonstore.jsonstore",
        "runs": true
    },
    {
        "file": "plugins/cordova-plugin-networkinterface/www/networkinterface.js",
        "id": "cordova-plugin-networkinterface.networkinterface",
        "clobbers": [
            "window.networkinterface"
        ]
    },
    {
        "file": "plugins/cordova-plugin-screen-orientation/www/screenorientation.js",
        "id": "cordova-plugin-screen-orientation.screenorientation",
        "clobbers": [
            "cordova.plugins.screenorientation"
        ]
    },
    {
        "file": "plugins/cordova-plugin-screen-orientation/www/screenorientation.ios.js",
        "id": "cordova-plugin-screen-orientation.screenorientation.ios",
        "merges": [
            "cordova.plugins.screenorientation"
        ]
    },
    {
        "file": "plugins/cordova.plugins.diagnostic.api-22/www/ios/diagnostic.js",
        "id": "cordova.plugins.diagnostic.api-22.Diagnostic",
        "clobbers": [
            "cordova.plugins.diagnostic"
        ]
    },
    {
        "file": "plugins/ionic-plugin-keyboard/www/ios/keyboard.js",
        "id": "ionic-plugin-keyboard.keyboard",
        "clobbers": [
            "cordova.plugins.Keyboard"
        ],
        "runs": true
    },
    {
        "file": "plugins/org.apache.cordova.battery-status/www/battery.js",
        "id": "org.apache.cordova.battery-status.battery",
        "clobbers": [
            "navigator.battery"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.console/www/console-via-logger.js",
        "id": "org.apache.cordova.console.console",
        "clobbers": [
            "console"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.console/www/logger.js",
        "id": "org.apache.cordova.console.logger",
        "clobbers": [
            "cordova.logger"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.device-motion/www/Acceleration.js",
        "id": "org.apache.cordova.device-motion.Acceleration",
        "clobbers": [
            "Acceleration"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.device-motion/www/accelerometer.js",
        "id": "org.apache.cordova.device-motion.accelerometer",
        "clobbers": [
            "navigator.accelerometer"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.splashscreen/www/splashscreen.js",
        "id": "org.apache.cordova.splashscreen.SplashScreen",
        "clobbers": [
            "navigator.splashscreen"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.statusbar/www/statusbar.js",
        "id": "org.apache.cordova.statusbar.statusbar",
        "clobbers": [
            "window.StatusBar"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.vibration/www/vibration.js",
        "id": "org.apache.cordova.vibration.notification",
        "merges": [
            "navigator.notification",
            "navigator"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.device/www/device.js",
        "id": "org.apache.cordova.device.device",
        "clobbers": [
            "device"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.dialogs/www/notification.js",
        "id": "org.apache.cordova.dialogs.notification",
        "merges": [
            "navigator.notification"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.geolocation/www/Coordinates.js",
        "id": "org.apache.cordova.geolocation.Coordinates",
        "clobbers": [
            "Coordinates"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.geolocation/www/PositionError.js",
        "id": "org.apache.cordova.geolocation.PositionError",
        "clobbers": [
            "PositionError"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.geolocation/www/Position.js",
        "id": "org.apache.cordova.geolocation.Position",
        "clobbers": [
            "Position"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.geolocation/www/geolocation.js",
        "id": "org.apache.cordova.geolocation.geolocation",
        "clobbers": [
            "navigator.geolocation"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.globalization/www/GlobalizationError.js",
        "id": "org.apache.cordova.globalization.GlobalizationError",
        "clobbers": [
            "window.GlobalizationError"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.globalization/www/globalization.js",
        "id": "org.apache.cordova.globalization.globalization",
        "clobbers": [
            "navigator.globalization"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.inappbrowser/www/inappbrowser.js",
        "id": "org.apache.cordova.inappbrowser.inappbrowser",
        "clobbers": [
            "window.open"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.network-information/www/network.js",
        "id": "org.apache.cordova.network-information.network",
        "clobbers": [
            "navigator.connection",
            "navigator.network.connection"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.network-information/www/Connection.js",
        "id": "org.apache.cordova.network-information.Connection",
        "clobbers": [
            "Connection"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "com.mirasense.scanditsdk.plugin": "4.13.4",
    "cordova-plugin-android-support-v4": "21.0.1",
    "cordova-plugin-insomnia": "4.2.0",
    "cordova-plugin-mfp": "7.1.0",
    "cordova-plugin-mfp-jsonstore": "7.1.0",
    "cordova-plugin-mfp-push": "7.1.0",
    "cordova-plugin-networkinterface": "1.0.8",
    "cordova-plugin-screen-orientation": "1.4.0",
    "cordova.plugins.diagnostic.api-22": "2.3.10-api-22",
    "ionic-plugin-keyboard": "2.2.1",
    "org.apache.cordova.battery-status": "0.2.12",
    "org.apache.cordova.console": "0.2.12",
    "org.apache.cordova.device-motion": "0.2.11",
    "org.apache.cordova.splashscreen": "0.3.5",
    "org.apache.cordova.statusbar": "0.1.9",
    "org.apache.cordova.vibration": "0.3.12",
    "org.apache.cordova.device": "0.2.13",
    "org.apache.cordova.dialogs": "0.2.11",
    "org.apache.cordova.geolocation": "0.3.11",
    "org.apache.cordova.globalization": "0.3.3",
    "org.apache.cordova.inappbrowser": "0.5.4",
    "org.apache.cordova.network-information": "0.2.14"
}
// BOTTOM OF METADATA
});