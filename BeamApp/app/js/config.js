(function () {
    'use strict';

    angular
        .module('beam')
        .config(config);
    config.$inject = ['$stateProvider', '$urlRouterProvider']

    function config($stateProvider, $urlRouterProvider){
        $stateProvider
            .state('login', {
                url: '/',
                    templateUrl: 'login.html',
                    controller: 'loginController',
                    controllerAs: 'vm'              
            })
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'menu.html',
                controller: 'menuCtrl',
                controllerAs: 'vm'
            })
            .state('app.loadreport', {
                cache: false,
                url: '/loadreport',
                views: {
                    'menuContent': {
                        templateUrl: 'loadReport.html',
                        controller: 'loadReportCtrl',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('app.detail', {
                cache: false,
                url: '/detail/:stopIdentifier/:type',
                views: {
                    'menuContent': {
                        templateUrl: 'pieceDetail.html',
                        controller: 'detailCtrl',
                        controllerAs: 'vm'

                    }
                }
            })
            .state('app.networkCheck', {
                cache: false,
                url: '/networkCheck',
                views: {
                    'menuContent': {
                        templateUrl: 'networkCheck.html',
                        controller: 'networkCheckCtrl',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('app.pickupDetail', {
                cache: false,
                url: '/pickupDetail',
                views: {
                    'menuContent': {
                        templateUrl: 'pickupDetail.html',
                        controller: 'pickupDetailCtrl',
                        controllerAs: 'vm',                       
                    }
                }
            })
            .state('app.unassignPickupDetail', {
                cache: false,
                url: '/unassignPickupDetail',
                views: {
                    'menuContent': {
                        templateUrl: 'unassignPickupDetail.html',
                        controller: 'pickupDetailCtrl',
                        controllerAs: 'vm',                       
                    }
                }
            })
            .state('app.manifest', {
                cache: false,
                url: '/manifest',
                views: {
                    'menuContent': {
                        templateUrl: 'manifest.html',
                        controller: 'ManifestCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.seqmanifest', {
                //cache: false,
                url: '/seqmanifest',
                views: {
                    'menuContent': {
                        templateUrl: 'seqViewManifest.html',
                        controller: 'seqManifestCtrl',
                        controllerAs: 'vm'                       
                    }
                }
            })
             .state('app.scan', {
                //cache: false,
                url: '/scan',
                views: {
                    'menuContent': {
                        templateUrl: 'scan.html',
                        controller: 'scanCtrl',
                        controllerAs: 'vm'                   
                    }
                }
            })
             .state('app.scan.stop', {
                //cache: false,
                url: '/selectScan/:stopIdentifier',
                views: {
                        'dialer' :{
                        templateUrl: "selectScan.html"
                    }
                }
            })
            .state('app.scan.load', {
                //cache: false,
                url: "/load/:stopIdentifier",
                views: {
                        'dialer' :{
                        templateUrl: "loadScan.html"
                    }
                }
            })
            .state('app.scan.delivery', {
                //cache: false,
                url: "/delivery/:stopIdentifier",
                views: {
                        'dialer' :{
                        templateUrl: "deliveryScan.html"
                    }
                }
            })
            .state('app.scan.attempt', {
                //cache: false,
                url: "/attempt/:stopIdentifier",
                views: {
                        'dialer' :{
                        templateUrl: "attemptScan.html"
                    }
                }
            })
            .state('app.scan.location', {
                //cache: false,
                url: "/location/:stopIdentifier",
                views: {
                        'dialer' :{
                        templateUrl: "locationScan.html"
                    }
                }
            })
            .state('app.scan.pickup', {
                //cache: false,
                url: "/pickup/:stopIdentifier",
                views: {
                        'dialer' :{
                        templateUrl: "pickupScan.html"
                    }
                }
            })
            .state('app.scan.door', {
                //cache: false,
                url: "/door/:stopIdentifier",
                views: {
                        'dialer' :{
                        templateUrl: "doorScan.html"
                    }
                }
            })    
            .state('app.signature', {
                cache: false,
                url: '/signature/:stopIdentifier',
                views: {
                    'menuContent': {
                        templateUrl: 'signature.html',
                        controller: 'signatureCtrl',
                        controllerAs: 'vm'                      
                    }
                }
            })
            .state('app.preferences', {
                cache: false,
                url: '/preferences',
                views: {
                    'menuContent': {
                        templateUrl: 'preferences.html',
                        controller: 'preferenceCtrl',
                        controllerAs: 'vm'                      
                    }
                }
            });
           
        $urlRouterProvider.otherwise('/');
    }
})();
