(function () {
    'use strict';
    angular
        .module('beam')
        .config(['$provide', function ($provide) {
            $provide.decorator('$log', ['$delegate', 'logService', function ($delegate, logService) {
                logService.enabled = true;
                var methods = {
                    error: function () {
                        if (logService.enabled) {
                            $delegate.error.apply($delegate, arguments);
                            logService.error.apply(null, arguments);
                        }
                    },
                    log: function () {
                        if (logService.enabled) {
                            $delegate.log.apply($delegate, arguments);
                        }
                    },
                    info: function () {
                        if (logService.enabled) {
                            $delegate.info.apply($delegate, arguments);
                        }
                    },
                    warn: function () {
                        if (logService.enabled) {
                            $delegate.warn.apply($delegate, arguments);
                        }
                    },
                    debug: function () {
                        if (logService.enabled) {
                            $delegate.debug.apply($delegate, arguments);
                        }
                    }
                };
                return methods;
            }]);
        }]);

})();