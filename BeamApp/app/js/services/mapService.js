angular
.module('beam')
.service('mapService', mapService);
mapService.$inject = ['$q', 'deviceService','notificationService'];
function mapService($q, deviceService,notificationService) {
	this.mapWithRouting = mapWithRouting;
	this.destinationPieceMap = destinationPieceMap;
	this.destinationSeqMap = destinationSeqMap;
	
	function mapWithRouting(originlatlog,destlatlog){
		var def = $q.defer();
		var connectionStatus = deviceService.getConnectionStatus();
		if (typeof google === 'object' && typeof google.maps === 'object' && connectionStatus == true) {  		
			var rendererOptions = { 
				map: map, 
				suppressMarkers : true 
			}         
			var directionsService = new google.maps.DirectionsService();
			var directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
			var map = new google.maps.Map(document.getElementById('map'), {
				disableDefaultUI:true,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});
			directionsDisplay.setMap(map);			   
			var request = {
				origin: {lat: originlatlog.lat, lng: originlatlog.lng}, 
				destination: {lat: destlatlog.lat, lng: destlatlog.lng},
				travelMode: google.maps.DirectionsTravelMode.DRIVING
			};
			var markersource=new google.maps.Marker({
				map : map,
				position:request.origin,
				icon:'img/ic_map_icon_xsm.png'
			});			        
			var markerdest=new google.maps.Marker({
				map : map,
				position:request.destination,
				icon:'img/stop_point_icon_xsm.png'
			});
			markersource.setMap(map);
			markerdest.setMap(map);
			directionsService.route(request, function(response, status) {
				if (status == google.maps.DirectionsStatus.OK) {
					directionsDisplay.setDirections(response);
					google.maps.event.addListenerOnce(map, 'idle', function () {
						var center = map.getCenter();
				        google.maps.event.trigger(map, 'resize');
				        map.setCenter(center);
				    });
				}else{
					destinationSeqMap(request.destination);
				}
			}); 
			def.resolve("true");  
		}else{
			def.reject("false");
		}
		return def.promise;
	}

	function destinationPieceMap(destlatlog){
		var def = $q.defer();
		var connectionStatus = deviceService.getConnectionStatus();
		if (typeof google === 'object' && typeof google.maps === 'object' && connectionStatus == true) {  		
			
		  	var myLatLng = {lat: destlatlog.lat, lng: destlatlog.lng};
			var map = new google.maps.Map(document.getElementById('mapload'), {
				center: myLatLng,
				zoom: 16
			});
			var markerdest=new google.maps.Marker({
				map : map,
				position:myLatLng,
				icon:'img/stop_point_icon_xsm.png'
			});
			google.maps.event.addListenerOnce(map, 'idle', function () {
		        google.maps.event.trigger(map, 'resize');
		        map.setCenter(myLatLng);
		    });
			def.resolve("true");  
		}else{
			def.reject("false");
		}
		return def.promise;
	}

	function destinationSeqMap(destlatlog){
		var def = $q.defer();
		var connectionStatus = deviceService.getConnectionStatus();
		if (typeof google === 'object' && typeof google.maps === 'object' && connectionStatus == true) {  		
			
		  	var myLatLng = {lat: destlatlog.lat, lng: destlatlog.lng};
			var map = new google.maps.Map(document.getElementById('map'), {
				center: myLatLng,
				zoom: 16
			});
			var markerdest=new google.maps.Marker({
				map : map,
				position:myLatLng,
				icon:'img/stop_point_icon_xsm.png'
			});	
			google.maps.event.addListenerOnce(map, 'idle', function () {
		        google.maps.event.trigger(map, 'resize');
		        map.setCenter(myLatLng);
		    });
			def.resolve("true");  
		}else{
			def.reject("false");
		}
		return def.promise;
	}
}