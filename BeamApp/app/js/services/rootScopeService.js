(function () {
    'use strict';

    angular
        .module('beam')
        .factory('rootScopeService', rootScopeService);
        rootScopeService.$inject = ['$window'];
        function rootScopeService($window) {

        	var service = {
	            saveLoadItems: saveLoadItems,
                saveUnasignLoadItems: saveUnasignLoadItems,
                saveDeliveryScanItems: saveDeliveryScanItems,
                saveAttemptScanItems: saveAttemptScanItems,
                savePickupScanItems: savePickupScanItems,
                saveLocationScanItems: saveLocationScanItems,
                saveUnassignPickupScanItems: saveUnassignPickupScanItems,
                saveDoorScanItems: saveDoorScanItems,
                getLoadItems: getLoadItems,
                getUnasignLoadItems: getUnasignLoadItems,
                getDeliveryScanItems: getDeliveryScanItems,
                getAttemptScanItems: getAttemptScanItems,
                getPickupScanItems: getPickupScanItems,
                getLocationScanItems: getLocationScanItems,
                getUnassignPickupScanItems: getUnassignPickupScanItems,
                getDoorScanItems: getDoorScanItems,
                clearLocalStorage: clearLocalStorage,
                clearDriverProfile: clearDriverProfile,
                saveDoorScanItemBarcode: saveDoorScanItemBarcode,
                getDoorScanItemBarcode: getDoorScanItemBarcode,
                saveScannedLoadScanItems: saveScannedLoadScanItems,
                getScannedLoadItems: getScannedLoadItems,
                saveUnknownLoadItems: saveUnknownLoadItems,
                getUnknownLoadItems : getUnknownLoadItems,
                saveLoadBarcodes : saveLoadBarcodes,
                savePickupBarcodes : savePickupBarcodes,
                clearLocalStorageVariables : clearLocalStorageVariables,
                clearDeliveryLocalVariables : clearDeliveryLocalVariables,
                clearLoadedLocalVariables : clearLoadedLocalVariables,
                unassignLoadItems : unassignLoadItems,
                getScannedUnassignLoadItems : getScannedUnassignLoadItems
	        };
	        
        	return service;

        	function saveLoadItems(loadScanItems){
        		$window.localStorage.loadScanItems = JSON.stringify(loadScanItems);
        	}

            function saveUnasignLoadItems(unassignLoadScanItems){
                $window.localStorage.unassignLoadScanItems = JSON.stringify(unassignLoadScanItems);
            }

            function saveDeliveryScanItems(deliveryScanItems){
                $window.localStorage.deliveryScanItems = JSON.stringify(deliveryScanItems);
            }

            function saveAttemptScanItems(attemptScanItems){
                $window.localStorage.attemptScanItems = JSON.stringify(attemptScanItems);
            }

            function savePickupScanItems(pickupScanItems){
                $window.localStorage.pickupScanItems = JSON.stringify(pickupScanItems);
            }

            function saveLocationScanItems(locationScanItems){
                $window.localStorage.locationScanItems = JSON.stringify(locationScanItems);
            }

            function saveUnassignPickupScanItems(unassignPickupScanItems){
                $window.localStorage.unassignPickupScanItems = JSON.stringify(unassignPickupScanItems);
            }

            function saveDoorScanItems(doorScanItems){
                $window.localStorage.doorScanItems = JSON.stringify(doorScanItems);
            }

            function saveDoorScanItemBarcode(doorScanBarcode){
                $window.localStorage.doorScanItemBarcode = JSON.stringify(doorScanBarcode);
            }

            function saveScannedLoadScanItems(scannedLoadScanBarcode){
               $window.localStorage.saveScannedLoadItems = JSON.stringify(scannedLoadScanBarcode);
            }

            function saveUnknownLoadItems(unknownLoadScanItems){
               $window.localStorage.unknownLoadScanItems = JSON.stringify(unknownLoadScanItems);
            }

            function saveLoadBarcodes(loadBarcodes){
               $window.localStorage.loadBarcodes = JSON.stringify(loadBarcodes);
            }

            function savePickupBarcodes(pickupBarcodes){
               $window.localStorage.pickupBarcodes = JSON.stringify(pickupBarcodes);
            }

            function unassignLoadItems(unassignItems){
               $window.localStorage.unassignLoadItems = JSON.stringify(unassignItems);
            }

            function getLoadItems(){
                var _loadScanItems = [];
                if (!$window.localStorage.loadScanItems){
                    _loadScanItems = [];
                }else{
                    angular.forEach(JSON.parse($window.localStorage.loadScanItems), function (piece, index) {
                        _loadScanItems.push({
                            barcode : piece.barcode,
                            scannedTime:new Date(piece.scannedTime)
                          })
                    });
                }
                return _loadScanItems;
            }

            function getUnasignLoadItems(){
                var _unasignLoadItems = [];
                if (!$window.localStorage.unassignLoadScanItems){
                    _unasignLoadItems = [];
                }else{
                    angular.forEach(JSON.parse($window.localStorage.unassignLoadScanItems), function (piece, index) {
                     _unasignLoadItems.push({
                            barcode : piece.barcode,
                            scannedTime:new Date(piece.scannedTime)
                          })
                    });
                }
                return _unasignLoadItems;
            }

            function getDeliveryScanItems(){
                var _deliveryItems = [];
                if (!$window.localStorage.deliveryScanItems){
                    _deliveryItems = [];
                }else{
                   angular.forEach(JSON.parse($window.localStorage.deliveryScanItems), function (piece, index) {
                     _deliveryItems.push({
                            barcode : piece.barcode,
                            scannedTime:new Date(piece.scannedTime)
                          })
                    });
               }
                return _deliveryItems;
            }

            function getAttemptScanItems(){
                 var _attemptItems = [];
                if (!$window.localStorage.attemptScanItems){
                    _attemptItems = [];
                }else{
                 angular.forEach(JSON.parse($window.localStorage.attemptScanItems), function (piece, index) {
                     _attemptItems.push({
                            barcode : piece.barcode,
                            scannedTime:new Date(piece.scannedTime)
                          })
                    });
                }
                return _attemptItems;
            }

            function getPickupScanItems(){
                 var _pickupItems = [];
                if (!$window.localStorage.pickupScanItems){
                    _pickupItems = [];
                }else{
                 angular.forEach(JSON.parse($window.localStorage.pickupScanItems), function (piece, index) {
                     _pickupItems.push({
                            barcode : piece.barcode,
                            scannedTime:new Date(piece.scannedTime)
                          })
                    });
                }
                return _pickupItems;
            }

            function getLocationScanItems(){
                var _locationItems = [];
                if (!$window.localStorage.locationScanItems){
                    _locationItems = [];
                }else{
                 angular.forEach(JSON.parse($window.localStorage.locationScanItems), function (piece, index) {
                     _locationItems.push({
                            barcode : piece.barcode,
                            scannedTime:new Date(piece.scannedTime)
                          })
                    });
                }
                return _locationItems;
            }

            function getUnassignPickupScanItems(){
                var _unassignPickupItems = [];
                if (!$window.localStorage.unassignPickupScanItems){
                    _unassignPickupItems = [];
                }else{
                 angular.forEach(JSON.parse($window.localStorage.unassignPickupScanItems), function (piece, index) {
                     _unassignPickupItems.push({
                            barcode : piece.barcode,
                            scannedTime:new Date(piece.scannedTime)
                          })
                    });
                }
                return _unassignPickupItems;
            }

            function getDoorScanItems(){
                var _doorScanItemsList = [];
                if (!$window.localStorage.doorScanItems){
                    _doorScanItemsList = [];
                }else{
                 angular.forEach(JSON.parse($window.localStorage.doorScanItems), function (piece, index) {
                     _doorScanItemsList.push({
                            barcode : piece.barcode,
                            scannedTime:new Date(piece.scannedTime)
                          })
                    });
                }
                return _doorScanItemsList;
            }

            function getDoorScanItemBarcode(){
                var _doorScanItemBarcode = [];
                if (!$window.localStorage.doorScanItemBarcode){
                    _doorScanItemBarcode = [];
                }else{
                 angular.forEach(JSON.parse($window.localStorage.doorScanItemBarcode), function (piece, index) {
                     _doorScanItemBarcode.push({
                            barcode : piece,
                            scannedTime:new Date()
                          })
                    });
                }
                return _doorScanItemBarcode;
            }


            function getScannedLoadItems(){
               var _scannedLoadScanItems = [];
               if (!$window.localStorage.saveScannedLoadItems){
                   _scannedLoadScanItems = [];
               }else{
                   angular.forEach(JSON.parse($window.localStorage.saveScannedLoadItems), function (piece, index) {
                       _scannedLoadScanItems.push({
                            barcode : piece.json.barcode,
                            scannedTime:new Date(piece.json.scannedTime)
                         })
                   });
               }
               return _scannedLoadScanItems;
           }

           function getScannedUnassignLoadItems(){
               var _unassignLoadItems = [];
               if (!$window.localStorage.unassignLoadItems){
                   _unassignLoadItems = [];
               }else{
                   angular.forEach(JSON.parse($window.localStorage.unassignLoadItems), function (piece, index) {
                       _unassignLoadItems.push({
                            barcode : piece.json.barcode,
                            scannedTime:new Date(piece.json.scannedTime)
                         })
                   });
               }
               return _unassignLoadItems;
           }

           function getUnknownLoadItems(){
                var _unknownLoadItems = [];
                if (!$window.localStorage.unknownLoadScanItems){
                    _unknownLoadItems = [];
                }else{
                 angular.forEach(JSON.parse($window.localStorage.unknownLoadScanItems), function (piece, index) {
                     _unknownLoadItems.push({
                            barcode : piece.barcode,
                            scannedTime:new Date(piece.scannedTime)
                          })
                    });
                }
                return _unknownLoadItems;
            }

            function clearLocalStorage(){
                $window.localStorage.loadScanItems = [];
                $window.localStorage.unassignLoadScanItems = [];
                $window.localStorage.deliveryScanItems = [];
                $window.localStorage.attemptScanItems = [];           
                $window.localStorage.pickupScanItems = [];   
                $window.localStorage.locationScanItems = [];
                $window.localStorage.unassignPickupScanItems = [];
                $window.localStorage.doorScanItems = []; 
                $window.localStorage.doorScanItemBarcode = [];  
            }

            function clearDriverProfile(){
                $window.localStorage.beamCredentials = [];
            }

            function clearLocalStorageVariables(){
                $window.localStorage.removeItem("loadScanItems");
                $window.localStorage.removeItem("unassignLoadScanItems");
                $window.localStorage.removeItem("deliveryScanItems");
                $window.localStorage.removeItem("attemptScanItems");
                $window.localStorage.removeItem("pickupScanItems");
                $window.localStorage.removeItem("locationScanItems");
                $window.localStorage.removeItem("unassignPickupScanItems");
                $window.localStorage.removeItem("doorScanItems");
                $window.localStorage.removeItem("doorScanItemBarcode");
            }

            function clearLoadedLocalVariables(){
                $window.localStorage.removeItem("saveScannedLoadItems");
                $window.localStorage.removeItem("unassignLoadItems");
                $window.localStorage.removeItem("loadBarcodes");
                $window.localStorage.removeItem("pickupBarcodes");
                $window.localStorage.removeItem("unknownLoadScanItems");
            }

            function clearDeliveryLocalVariables(){
                $window.localStorage.removeItem("droplocationcode");
                $window.localStorage.removeItem("droplocationvalue");
                $window.localStorage.removeItem("podname");
                $window.localStorage.removeItem("signature");
                $window.localStorage.removeItem("signatureRequired");
            }
        }

})();