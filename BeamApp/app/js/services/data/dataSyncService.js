(function () {
    'use strict';

    angular
        .module('beam')
        .factory('dataSyncService', dataSyncService);

    dataSyncService.$inject = ['$cordovaNetwork', '$window', '$interval','eventsService','SyncStats','deviceEventsService','logService'];
    function dataSyncService($cordovaNetwork, $window, $interval,eventsService,SyncStats,deviceEventsService,logService) {
        var service = {
            registerSync: registerSync,
            removeSync: removeSync,
            locationPoll: locationPoll,
            syncEvents: syncEvents,
            cancelLocationPoll: cancelLocationPoll,
            startEventSyncr:startEventSyncr,
            stopEventSyncr:stopEventSyncr
        };
        var _poll,_eventSyncr;
        return service;

        function registerSync() {
            $window.addEventListener('online', handleConnectionUp, false);
            $window.addEventListener('offline', handleConnectionDown, false);
        }
        function handleConnectionUp() {
            console.log('### Connection UP ##',SyncStats);
            syncEvents();
        }
        function handleConnectionDown() {
            console.log('### Connection Down ##');
        }
        function syncEvents() {
            if(!SyncStats.synced && $cordovaNetwork.isOnline() && !SyncStats.syncInProgress){
                eventsService.sendEvents();
                SyncStats.synced = true;
            }
        }
        function locationPoll(time) {
            if (!time)
                time = 300;
            if (!_poll)
                _poll = $interval(function () {
                    deviceEventsService.sendLocation();
                }, time*1000, 0, false);
        }
        function cancelLocationPoll() {
            if (angular.isDefined(_poll)) {
                $interval.cancel(_poll);
                _poll = undefined;
            }
        }
        function removeSync() {
            $window.removeEventListener('online', handleConnectionUp, false);
            $window.removeEventListener('offline', handleConnectionDown, false);
        }
        function startEventSyncr(){
            SyncStats.synced = false;
            SyncStats.syncInProgress = false;
            _eventSyncr = $interval(function(){
                syncEvents();
                sendLogs();
            },60*1000,0,false);
        }
        function stopEventSyncr(){
            $interval.cancel(_eventSyncr);
        }
        function sendLogs(){
            logService.logs.forEach(function(logItem) {
                deviceEventsService.sendLog(logItem.message,logItem.time,logItem.type);
            }, this);
            logService.logs = [];
        }
    }
})();