(function () {
    'use strict';
    angular
        .module('beam')
        .service('logService', function ($injector) {
            var service = {
                error: function () {
                    self.type = 'error';
                    log.apply(self, arguments);
                    WL.Logger.ctx({ pkg: 'BeamApp', autoSendLogs: true }).error(arguments);
                },
                warn: function () {
                    self.type = 'warn';
                    log.apply(self, arguments);
                    WL.Logger.ctx({ pkg: 'BeamApp', autoSendLogs: true }).warn(arguments);
                },
                info: function () {
                    self.type = 'info';
                    log.apply(self, arguments);
                    WL.Logger.ctx({ pkg: 'BeamApp', autoSendLogs: true }).info(arguments);
                },
                log: function () {
                    self.type = 'log';
                    log.apply(self, arguments);
                    WL.Logger.ctx({ pkg: 'BeamApp', autoSendLogs: true }).log(arguments);
                },
                debug: function () {
                    self.type = 'debug';
                    log.apply(self, arguments);
                    WL.Logger.ctx({ pkg: 'BeamApp', autoSendLogs: true }).debug(arguments);
                },
                enabled: false,
                logs: []
            };

            var log = function () {

                var args = [];
                if (typeof arguments === 'object') {
                    for (var i = 0; i < arguments.length; i++) {
                        var arg = arguments[i];
                        var exception = {};
                        if (arg.message && arg.stack) {
                            exception.message = arg.message;
                            exception.stack = arg.stack;
                            args.push(JSON.stringify(exception));
                        }
                        else
                            args.push(JSON.stringify(arg));

                    }
                }
                else if (angular.isArray(arguments)) {
                    args = arguments[0];
                }

                var eventLogDateTime = new Date();
                var logItem = {
                    time: eventLogDateTime,
                    message: encodeURI(args.join('\n')),
                    type: type.toUpperCase()
                };
                service.logs.push(logItem);
            };


            return service;

        });

})();