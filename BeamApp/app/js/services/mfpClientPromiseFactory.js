(function () {
  'use strict';

  angular
    .module('beam')
    .factory('MFPClientPromise', MFPClientPromise);

  MFPClientPromise.$inject = ['$q'];
  function MFPClientPromise($q) {
    /* Setup a Promise to allow code to run in other places anytime after MFP CLient SDK is ready
       Example: MFPClientPromise.then(function(){alert('mfp is ready, go ahead and use WL.* APIs')});
    */
    return window.MFPClientDefer.promise;
  }
})();
