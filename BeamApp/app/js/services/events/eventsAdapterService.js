(function () {
    'use strict';

    angular
        .module('beam')
        .factory('eventsAdapterService', eventsAdapterService);

    eventsAdapterService.$inject = ['$q', 'SyncStats', '$log'];
    function eventsAdapterService($q, SyncStats, $log) {
        var service = {
            sendEvents: sendEventsSerial,
            sendDataToAdapter: sendDataToAdapter
        };
        var _serverResponses = [];
        return service;


        function sendEvents(eventsData) {
            var promises = [];
            var deferred = $q.defer();
            eventsData.forEach(function (event) {
                promises.push(sendDataToAdapter(event));
            }, this);
            $q.all(promises).then(function (responses) {
                SyncStats.synced = false;
                SyncStats.lastSynced = Date();
                deferred.resolve(responses);
            }, function (err) {
                SyncStats.synced = false;
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function sendEventsSerial(eventsData) {
            var promises = [];
            var deferred = $q.defer();
            if (SyncStats.syncInProgress) {
                SyncStats.synced = false;
                deferred.resolve([]);
                console.info('Send in progress. Skipping send request');
            }
            else {
                _serverResponses=[];
                SyncStats.syncInProgress = true;
                console.info('Sending Events @'+ new Date());
                doAsyncInSeries(eventsData).then(function (results) {
                    console.log('Sending Events Done @' + new Date(), _serverResponses.length);
                    SyncStats.synced = false;
                    SyncStats.lastSynced = Date();
                    SyncStats.syncInProgress = false;
                    deferred.resolve(_serverResponses);
                });
            }
            return deferred.promise;
        }

        function doAsyncInSeries(arr) {
            return arr.reduce(function (promise, item) {
                return promise.then(function (result) {
                    return sendDataToAdapter(item);
                });
            }, $q.when(null));
        }

        function sendDataToAdapter(event) {
            var deferred = $q.defer();
            var pEvent = event;
            if(pEvent.json.Events.length>5){
                pEvent.json.Events = event.json.Events.slice(0,5);
            }
            var sendEventsReq = new WLResourceRequest(
                "/adapters/Events/sendEvents",
                WLResourceRequest.POST
            );
            sendEventsReq.setQueryParameter("params", [pEvent]);
            sendEventsReq.send().then(function (response) {
                if (response.status == 200 && response.responseJSON.isSuccessful) {
                    deferred.resolve(response.responseJSON.adapterResponse);
                    _serverResponses.push(response.responseJSON.adapterResponse);
                }
                else {
                    $log.error('Event Send Err from Adapter', response);
                    deferred.resolve([]);
                }
            }, function (err) {
                $log.error('Error sending event to adapter : ' + JSON.stringify(event), err);
                deferred.resolve([]);
            });
            return deferred.promise;
        }
    }
})();