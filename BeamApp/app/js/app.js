// LaserShip's Beam App
(function() {
    'use strict';

    angular.module('beam', [
        'ionic',
        'ngCordova',
        'angular.filter',
        'underscore'
    ]);
})();